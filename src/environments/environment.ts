// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAL6AYvNfOdVGJrGSPmn0BPOjENw_jUxh8",
    authDomain: "test-730f7.firebaseapp.com",
    databaseURL: "https://test-730f7.firebaseio.com",
    projectId: "test-730f7",
    storageBucket: "test-730f7.appspot.com",
    messagingSenderId: "611011301868"
  }
};
