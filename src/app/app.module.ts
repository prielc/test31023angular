import { ProductsService } from './products/products.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { environment } from './../environments/environment';



import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProductUpdateFormComponent } from './products/product-update-form/product-update-form.component';
import { FproductsComponent } from './fproducts/fproducts.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    ProductUpdateFormComponent,
    FproductsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule, 
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'', component:ProductsComponent}, //default - localhost:4200 - homepage
      {path:'product-update-form/:id', component:ProductUpdateFormComponent},
      {path:'login', component:LoginComponent}, 
      {path:'fproducts', component:FproductsComponent},
      {path:'**', component:NotFoundComponent} //all the routs that don't exist
    ])
  ],
  providers: [
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
