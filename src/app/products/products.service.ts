import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { Headers } from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class ProductsService {

  getProducts(){
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/angular/Test2018/slim1/products');
  }
  getProductsFire(){
    return this.db.list('/products').valueChanges();    
  }

  getProduct(id) {
    return this.http.get('http://localhost/angular/Test2018/slim1/products/'+id);
  }

  putProduct(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put('http://localhost/angular/Test2018/slim1/products/'+ key,params.toString(), options);
  }

  constructor(private http:Http, private db:AngularFireDatabase) { 
    
  }

}
