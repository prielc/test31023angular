import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products;
  productsKeys = [];

  optimisticAdd(product){
    var newKey = this.productsKeys[this.productsKeys.length-1]+1;
    console.log(this.productsKeys.length);
    console.log(this.productsKeys);
    var newproductObject = {};
    newproductObject['body'] = product;
    console.log(newproductObject);
    this.products[newKey] = newproductObject;
    this.productsKeys = Object.keys(this.products);
    console.log(this.products);
    console.log(event,newKey);
  }

  pessimiaticAdd(){
    this.service.getProducts().subscribe(response => {
      this.products =  response.json();
      this.productsKeys = Object.keys(this.products);
    });   
  }

  constructor(private service:ProductsService) {
      this.service.getProducts().subscribe(response=>{
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
    });
   }

  ngOnInit() {
  }

}
