import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from './../products.service';

@Component({
  selector: 'app-product-update-form',
  templateUrl: './product-update-form.component.html',
  styleUrls: ['./product-update-form.component.css']
})
export class ProductUpdateFormComponent implements OnInit {

  @Output() addProduct:EventEmitter<any> = new EventEmitter<any>(); //any is the type. could be [] or {}
  @Output() addProductPs:EventEmitter<any> = new EventEmitter<any>(); //pessimistic

  product;
  service:ProductsService;

  productform = new FormGroup({
    name:new FormControl(),
    price:new FormControl(),
  });

  constructor(private route: ActivatedRoute ,service: ProductsService, private router: Router, private formBuilder:FormBuilder) {
    this.service = service;
   }

  sendData() { //הפליטה של העדכון לאב
    this.addProduct.emit(this.productform.value.name);
    console.log(this.productform.value);
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putProduct(this.productform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.addProductPs.emit();
          this.router.navigateByUrl("/");//return to main page
        }
      );
    })
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getProduct(id).subscribe(response=>{
        this.product = response.json();
        console.log(this.product);
      })
    })
    this.productform = this.formBuilder.group({
	      name:  [null, [Validators.required]],
	      price: [null, Validators.required],
	    });	
  }

}
